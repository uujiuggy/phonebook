package dao;

import domain.Contact;

import java.util.List;

public interface ContactDao extends GenericDao<Contact, Integer>{
    List<Contact> findByName(String name);

}
