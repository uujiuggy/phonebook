package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public abstract class AbstractJDBCDao<T extends Identified<PK>, PK
                                      extends Integer>
                                      implements GenericDao<T, PK> {
    private static final String PERSIST_ERROR = "On persist modify more then" +
                                                " 1 record: ";
    private static final String FIND_BY_PK_ERROR = "Exception on findByPK new" +
                                                   " persist data.";
    private static final String GET_BY_PK_ERROR = "Received more than one" +
                                                  " record.";
    private static final String UPDATE_ERROR = "On update modify more then" +
                                               " 1 record: ";
    private static final String DELETE_ERROR = "On delete modify more then" +
                                               " 1 record: ";

    private Connection connection;

    public abstract String getSelectQuery();

    public abstract String getCreateQuery();

    public abstract String getUpdateQuery();

    public abstract String getDeleteQuery();

    public abstract String getSearchTerm();

    public abstract String getCondition();

    protected abstract List<T> parseResultSet(ResultSet rs) throws PersistException;

    protected abstract void prepareStatementForInsert(PreparedStatement statement, T object) throws PersistException;

    protected abstract void prepareStatementForUpdate(PreparedStatement statement, T object) throws PersistException;

    @Override
    public T persist(T object) throws PersistException {
        T persistInstance;
        String sql = getCreateQuery();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForInsert(statement, object);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new PersistException(PERSIST_ERROR + count);
            }

        } catch (Exception e) {
            throw new PersistException(e);
        }

        sql = getSelectQuery() + getSearchTerm();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet rs = statement.executeQuery();
            List<T> list = parseResultSet(rs);
            if ((list == null) || (list.size() != 1)) {
                throw new PersistException(FIND_BY_PK_ERROR);
            }

            persistInstance = list.iterator().next();
        } catch (Exception e) {
            throw new PersistException(e);
        }

        return persistInstance;
    }

    @Override
    public T getByPK(Integer key) throws PersistException {
        List<T> list;
        String  sql = getSelectQuery();
        sql += getCondition();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, key);
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            throw new PersistException(e);
        }

        if (list == null || list.size() == 0) {
            throw new PersistException("Record with PK = " + key + " not found.");
        }

        if (list.size() > 1) {
            throw new PersistException(GET_BY_PK_ERROR);
        }

        return list.iterator().next();
    }

    @Override
    public void update(T object) throws PersistException {
        String sql = getUpdateQuery();
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            prepareStatementForUpdate(statement, object);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new PersistException(UPDATE_ERROR + count);
            }

        } catch (Exception e) {
            throw new PersistException(e);
        }

    }

    @Override
    public void delete(T object) throws PersistException {
        String sql = getDeleteQuery();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            try {
                statement.setObject(1, object.getId());
            } catch (Exception e) {
                throw new PersistException(e);
            }

            int count = statement.executeUpdate();

            if (count != 1) {
                throw new PersistException(DELETE_ERROR + count);
            }

        } catch (Exception e) {
            throw new PersistException(e);
        }

    }

    @Override
    public List<T> getAll() throws PersistException {
        List<T> list;
        String  sql = getSelectQuery();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            throw new PersistException(e);
        }

        return list;
    }

    public AbstractJDBCDao(Connection connection) {
        this.connection = connection;
    }

}
