package servise;

import dao.AddressesDao;
import dao.GenericDao;
import dao.PersistException;
import domain.Addresses;
import domain.Contact;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.query.QueryParameter;
import utils.SessionUtil;
import java.util.List;

public class HibernateAddresses extends SessionUtil implements AddressesDao {
    private static final String FIND_LAST_INSERT = "from Addresses where id = :addres_id";
    private static final String LAST_INSERT = "last_insert_id()";
    private static final String PK = "id";
    private static final String SELECT_BY_ID = "SELECT * FROM ADDRESSES WHERE ID = :address_id";
    private static final String SELECT_ALL = "SELECT * FROM ADDRESSES";
    private static final String SELECT_BY_CONTACT_ID = "SELECT * FROM ADDRESSES WHERE ID = :CONTACT_ID";
    private static final String CONTACT_ID = "ID";

    @Override
    public Addresses persist(Addresses object) throws PersistException {
        openTransactionSession();

        Session session = getSession();
        session.save(object);

        closeTransactionSesstion();

        return object;
    }

    @Override
    public Addresses getByPK(Integer id) throws PersistException {
        openTransactionSession();

        Session session = getSession();
        Query query = session.createNativeQuery(SELECT_BY_ID).addEntity(Addresses.class);
        query.setParameter(PK, id);
        Addresses addresses = (Addresses) query.getSingleResult();

        closeTransactionSesstion();

        return addresses;
    }

    @Override
    public void update(Addresses object) throws PersistException {
        openTransactionSession();
        Session session = getSession();
        session.update(object);

        closeTransactionSesstion();
    }

    @Override
    public void delete(Addresses object) throws PersistException {
        openTransactionSession();

        Session session = getSession();
        session.remove(object);

        closeTransactionSesstion();

    }

    @Override
    public List<Addresses> getAll() throws PersistException {
        openTransactionSession();

        Session session = getSession();
        Query query = session.createNativeQuery(SELECT_ALL).addEntity(Addresses.class);
        List<Addresses> addressList = query.list();

        closeTransactionSesstion();

        return addressList;
    }

    @Override
    public Addresses findByContactId(int contactId) {
        openTransactionSession();

        Session session = getSession();
        Query query = session.createNativeQuery(SELECT_BY_CONTACT_ID).addEntity(Addresses.class);
        query.setParameter(CONTACT_ID, contactId);
        Addresses address = (Addresses) query.getSingleResult();

        closeTransactionSesstion();

        return address;
    }

}
