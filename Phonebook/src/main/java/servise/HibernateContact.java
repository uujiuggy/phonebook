package servise;

import dao.ContactDao;
import dao.PersistException;
import domain.Contact;
import org.hibernate.Session;
import org.hibernate.query.Query;
import utils.SessionUtil;

import java.util.List;

public class HibernateContact extends SessionUtil implements ContactDao {
    private static final String FIND_LAST_INSERT = "from Contact where id = :contact_id";
    private static final String LAST_INSERT = "last_insert_id()";
    private static final String PK = "contact_id";
    private static final String SELECT_BY_ID = "SELECT * FROM CONTACT WHERE ID = :contact_id";
    private static final String SELECT_ALL = "SELECT * FROM CONTACT";
    private static final String SELECT_BY_NAME = "SELECT * FROM CONTACT WHERE NAME = :NAME";
    private static final String NAME = "NAME";

    @Override
    public Contact persist(Contact object) throws PersistException {
        openTransactionSession();

        Session session = getSession();
        session.save(object);

        closeTransactionSesstion();

        return object;
    }

    @Override
    public Contact getByPK(Integer id) throws PersistException {
        openTransactionSession();

        Session session = getSession();
        Query query = session.createNativeQuery(SELECT_BY_ID).addEntity(Contact.class);
        query.setParameter(PK, id);
        Contact contact = (Contact) query.getSingleResult();

        closeTransactionSesstion();

        return contact;
    }

    @Override
    public void update(Contact object) throws PersistException {
        openTransactionSession();

        Session session = getSession();
        session.update(object);


        closeTransactionSesstion();
    }

    @Override
    public void delete(Contact object) throws PersistException {
        openTransactionSession();

        Session session = getSession();
        session.remove(object);

        closeTransactionSesstion();

    }

    @Override
    public List<Contact> getAll() throws PersistException {
        openTransactionSession();

        Session session = getSession();
        Query query = session.createNativeQuery(SELECT_ALL).addEntity(Contact.class);
        List<Contact> addressList = query.list();

        closeTransactionSesstion();

        return addressList;
    }

    @Override
    public List<Contact> findByName(String name) {
        openTransactionSession();

        Session session = getSession();
        Query query = session.createNativeQuery(SELECT_BY_NAME).addEntity(Contact.class);
        query.setParameter(NAME, name);
        List<Contact> contacts = query.list();

        closeTransactionSesstion();

        return contacts;
    }
}
