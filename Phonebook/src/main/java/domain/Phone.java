package domain;



import com.sun.istack.NotNull;
import dao.Identified;
import javax.persistence.*;

@Entity
@Table(name = "PHONE")
public class Phone implements Identified<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull
    @Column(unique = true, name = "phone_number")
    private String phoneNumber;

    @Column(name = "type")
    private String type;

    @ManyToOne
    @JoinColumn(name = "contact_id")
    private Contact contact;

    public Phone() {}

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id=" + id +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

}
