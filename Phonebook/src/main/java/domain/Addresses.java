package domain;

import com.sun.istack.NotNull;
import dao.Identified;
import javax.persistence.*;

@Entity
@Table(name = "ADDRESSES")
public class Addresses implements Identified<Integer> {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "contact_id")
    private Contact contact;

    @Column(name = "address")
    private String address;

    public Addresses() {}

    public void setId(int id) {
        this.id = id;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Addresses{" +
                "id=" + id +
                ", address='" + address + '\'' +
                '}';
    }

}
