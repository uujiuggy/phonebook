package domain;

import dao.Identified;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;


@Entity
@Table(name = "contact")
public class Contact implements Identified<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String  name;

    @OneToMany(mappedBy = "contact", cascade = CascadeType.ALL)
    private List<Addresses> addresses;

    @OneToMany(mappedBy = "contact", cascade = CascadeType.ALL)
    private List<Phone> phones;

    public Contact() {}

    public Contact(String name, Addresses address, Phone phone) {
        setName(name);
        addAddress(address);
        addPhone(phone);
    }

    public void addAddress(Addresses address){
        addresses = new LinkedList<>();
        addresses.add(address);
    }

    public void  addPhone(Phone phone){
        phones = new LinkedList<>();
        phones.add(phone);
    }

    public List<Addresses> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Addresses> addresses) {
        this.addresses = addresses;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
