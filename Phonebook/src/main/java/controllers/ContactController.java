package controllers;

import dao.ContactDao;
import dao.PersistException;
import domain.Contact;
import servise.HibernateContact;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ContactController extends HttpServlet {
    private static final String JSP_PATH = "templates/Main.jsp";
    private static final String ADD_FAILURE = "Не удалось добавить пользователя";
    private static final String NAME_ATTRIBUTE = "name";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher(JSP_PATH);
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ContactDao hibernateContact = new HibernateContact();
        String name =(String) req.getAttribute(NAME_ATTRIBUTE);
        Contact contact = new Contact();
        contact.setName(name);
        try {
            hibernateContact.persist(contact);
        } catch (PersistException e) {
            System.out.println(ADD_FAILURE);
            e.printStackTrace();
        }

    }

}
