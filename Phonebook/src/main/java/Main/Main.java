package Main;

import dao.*;
import domain.Addresses;
import domain.Contact;
import domain.Phone;
import servise.HibernateAddresses;
import servise.HibernateContact;
import servise.HibernatePhone;
import utils.HibernateUtil;

import java.sql.SQLException;
import java.util.LinkedList;


public class Main {
    private static final String WORK_ADDRESS = "London, Baker street 221b";
    private static final String HOME_ADDRESS = "Concordia, 75°06'00.0\"S 123°21'00.0\"E";
    private static final String PHONE_NUMBER_1 = "333";
    private static final String PHONE_NUMBER_2 = "333111";
    private static final String PHONE_NUMBER_3 = "0956768768";
    private static final String PHONE_TYPE_1 = "work";
    private static final String PHONE_TYPE_2 = "fax";
    private static final String PHONE_TYPE_3 = "mobile";
    private static final String NAME = "Sergey";
    private static final String GENERATED_NAME = "Contact_";
    private static final int NUMBER_OF_ACCOUNTS = 1000;

    public static void main(String[] args) throws PersistException, SQLException {
        Phone workPhone = new Phone();
        Phone fax = new Phone();
        Phone newPhone = new Phone();
        Addresses workAddress = new Addresses();
        Addresses homeAddress = new Addresses();

        Contact contact = new Contact();
        LinkedList<Phone> phones = new LinkedList<>();
        fax.setPhoneNumber(PHONE_NUMBER_2);
        fax.setType(PHONE_TYPE_2);
        phones.add(workPhone);
        phones.add(fax);
        workAddress.setAddress(WORK_ADDRESS);
        homeAddress.setAddress(HOME_ADDRESS);
        homeAddress.setContact(contact);

        workPhone.setPhoneNumber(PHONE_NUMBER_1);
        workPhone.setType(PHONE_TYPE_1);
        contact.setName(NAME);
        contact.addAddress(workAddress);
        contact.setPhones(phones);
        fax.setContact(contact);
        workPhone.setContact(contact);
        workAddress.setContact(contact);
        newPhone.setContact(contact);
        newPhone.setPhoneNumber(PHONE_NUMBER_3);
        newPhone.setType(PHONE_TYPE_3);

        try {
            AddressesDao hibernateAddresses = new HibernateAddresses();
            ContactDao hibernateContact = new HibernateContact();
            PhoneDao hibernatePhone = new HibernatePhone();
            hibernateContact.persist(contact);
            hibernatePhone.persist(newPhone);
            hibernateAddresses.persist(homeAddress);

            contact.setName("Igor");
            hibernateContact.update(contact);

            workAddress.setAddress("London, Baker street 221c");
            hibernateAddresses.update(workAddress);

            workPhone.setPhoneNumber("3806677788");
            hibernatePhone.update(workPhone);

            System.out.println(hibernateAddresses.getAll());
            System.out.println(hibernateContact.getByPK(1));
            hibernatePhone.delete(fax);
            System.out.println(hibernatePhone.getAll());

            for (int i = 0; i < NUMBER_OF_ACCOUNTS; i++) {
                Contact newContact = new Contact();
                if (i % 100 == 0) {
                    newContact.setName(NAME);
                } else {
                    newContact.setName(GENERATED_NAME + i);
                }

                hibernateContact.persist(newContact);
            }

            System.out.println(hibernateContact.findByName(NAME));

            hibernateContact.delete(hibernateContact.getByPK(1));

        } finally {
            HibernateUtil.shutdown();
        }

    }

}
