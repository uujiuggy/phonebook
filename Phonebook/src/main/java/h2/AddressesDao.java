package h2;

import dao.AbstractJDBCDao;
import dao.PersistException;
import domain.Addresses;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;


public class AddressesDao extends AbstractJDBCDao<Addresses, Integer> {
    private static final String SELECT_QUERY = "SELECT user_id, city, street," +
                                               " building FROM Addresses";
    private static final String CREATE_QUERY = "INSERT INTO Addresses (user_id," +
                                               " city, street, building)" +
                                               " VALUES (?, ?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE Addresses SET city = ?" +
                                               " street = ?  building = ? " +
                                               "WHERE user_id = ?";
    private static final String DELETE_QUERY = "DELETE FROM ADDRESSES " +
                                               "WHERE user_id = ?";
    private static final String SEARCH_TERM = " where user_id = last_insert_id()";
    private static final String CONDITION = " WHERE id = ?";

    private class PersistAddress extends Addresses {
        public void setUserId(int id) {
            super.setId(id);
        }

    }

    @Override
    public String getSelectQuery() {
        return SELECT_QUERY;
    }

    @Override
    public String getCreateQuery() {
        return CREATE_QUERY;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE_QUERY;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE_QUERY;
    }

    @Override
    public String getSearchTerm() {
        return SEARCH_TERM;
    }

    @Override
    public String getCondition() {
        return CONDITION;
    }

    public AddressesDao(Connection connection) {
        super(connection);
    }

    @Override
    protected List<Addresses> parseResultSet(ResultSet rs) throws PersistException {
        List<Addresses> result = new LinkedList<>();
        try {
            while (rs.next()) {
                PersistAddress address = new PersistAddress();
                address.setUserId(rs.getInt("id"));
                address.setAddress(rs.getString("address"));
                result.add(address);
            }

        } catch (Exception e) {
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Addresses object) throws PersistException {
        try {
            statement.setInt(1, object.getId());
            statement.setString(2, object.getAddress());
        } catch (Exception e) {
            throw new PersistException(e);
        }

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Addresses object) throws PersistException {
        try {
            statement.setString(1, object.getAddress());
            statement.setInt(2, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }

    }

}
